import 'package:flame/components.dart';
import 'package:flame/flame.dart';

class WorldMap extends SpriteComponent {
  double posX;
  double posY;
  double tileSize = 256 / 8;
  int imgNr;

  WorldMap(this.posX, this.posY, this.imgNr);

  @override
  Future<void>? onLoad() async {
    if (imgNr == 0) {
      sprite = Sprite(await Flame.images.load('environment/world_tile0.png'));
    } else {
      sprite = Sprite(await Flame.images.load('environment/world_tile1.png'));
    }

    size = Vector2(tileSize, tileSize);
    position = Vector2(posX * 1 - tileSize * posX - tileSize / 2,
        posY * 1 - tileSize * posY - tileSize / 2);
    return super.onLoad();
  }
}
