import 'package:flame/components.dart';
import 'package:flutter/material.dart';

class FixedRectangle extends RectangleComponent {
  Vector2 screensize;

  FixedRectangle(this.screensize) : super(size: Vector2(40, 50)) {
    paint = Paint()..color = Colors.red[300]!;
    paint.strokeWidth = 1;
    paint.style = PaintingStyle.stroke;
    position = Vector2(
        screensize.x / 2 - size.x / 2, (screensize.y / 2 - size.y) + 25);
    positionType = PositionType.viewport;
  }
}
