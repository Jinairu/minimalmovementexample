import 'dart:math';

import 'package:flame/components.dart';
import 'package:flame/flame.dart';
import 'package:flame/sprite.dart';
import 'package:flame_forge2d/flame_forge2d.dart';
import 'package:flame_forge2d/position_body_component.dart';

class Player extends PositionBodyComponent {
  final double _playerSpeed = 20.0;

  Vector2 targetVector = Vector2.zero();

  SpriteAnimationComponent spriteAnimationComponent;
  late SpriteAnimation _idleAnimation;

  Player(this.spriteAnimationComponent)
      : super(
          positionComponent: spriteAnimationComponent,
          size: Vector2.all(4),
        );

  @override
  Future<void> onLoad() async {
    // Single sprite - will be replaced
    _idleAnimation = SpriteSheet(
      image: await Flame.images.load('character/player.png'),
      srcSize: Vector2(37, 40),
    ).createAnimation(row: 0, from: 0, to: 1, stepTime: 4);

    spriteAnimationComponent.animation = _idleAnimation;
    return super.onLoad();
  }

  @override
  void update(double dt) {
    super.update(dt);
    movePlayer(dt);
  }

  void movePlayer(double dt) {
    double vectX = targetVector.x - positionComponent!.position.x;
    double vectY = (positionComponent!.position.y + targetVector.y);
    double distance = positionComponent!.position.distanceTo(targetVector);
    Vector2 velocity =
        (Vector2(vectX, vectY) / distance) * dt * _playerSpeed * 100;
    body.applyLinearImpulse(velocity);

    // setting velocity directly
    // if (!targetVector.isZero()) {
    //   if (targetVector.y < 0) {
    //     targetVector.y = max(targetVector.y, -_playerSpeed);
    //   } else {
    //     targetVector.y = min(targetVector.y, _playerSpeed);
    //   }
    //   if (targetVector.x < 0) {
    //     targetVector.x = max(targetVector.x, -_playerSpeed);
    //   } else {
    //     targetVector.x = min(targetVector.x, _playerSpeed);
    //   }
    //   body.linearVelocity = targetVector;
    // } else {
    //   body.linearVelocity = Vector2.zero();
    // }
  }

  @override
  Body createBody() {
    PolygonShape shape = PolygonShape()..setAsBoxXY(1, 1.6);

    final fixtureDef = FixtureDef(shape);
    fixtureDef.restitution = 0;
    fixtureDef.density = 10;
    fixtureDef.friction = 10;
    fixtureDef.filter.maskBits = 0x0004 | 0x0008;
    fixtureDef.filter.categoryBits = 0x0002;

    final bodyDef = BodyDef();
    bodyDef.fixedRotation = true;
    bodyDef.userData = this;
    bodyDef.position = spriteAnimationComponent.position;
    bodyDef.type = BodyType.dynamic;

    return world.createBody(bodyDef)..createFixture(fixtureDef);
  }
}
