import 'dart:math';

import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:flame/input.dart';

import 'package:flutter/material.dart';
import 'package:flame_forge2d/forge2d_game.dart';

import 'components/fixedrectangle.dart';
import 'components/player.dart';
import 'components/world.dart';

class MageExample extends Forge2DGame with MultiTouchDragDetector {
  final List<List<WorldMap>> _worldList = [];

  late final Player _player;

  late DragStartDetails startDragDetails;

  @override
  Future<void> onLoad() async {
    double screenRatioV = canvasSize.x / canvasSize.y;
    double screenRatioH = canvasSize.y / canvasSize.x;

    camera.viewport = FixedResolutionViewport(Vector2(
        canvasSize.x - canvasSize.x * min(screenRatioH, screenRatioV),
        canvasSize.y - canvasSize.y * min(screenRatioH, screenRatioV)));

    world.setGravity(Vector2.zero());
    _worldList.addAll([
      [WorldMap(-1, -1, 0), WorldMap(0, -1, 0), WorldMap(1, -1, 0)],
      [WorldMap(-1, 0, 1), WorldMap(0, 0, 1), WorldMap(1, 0, 1)],
      [WorldMap(-1, 1, 0), WorldMap(0, 1, 0), WorldMap(1, 1, 0)]
    ]);

    for (List<WorldMap> maps in _worldList) {
      await addAll(maps);
    }

    _player = Player(SpriteAnimationComponent(
      size: Vector2.all(8),
      position: Vector2(16, 16),
    ));
    await add(_player);

    camera.followComponent(_player.spriteAnimationComponent);

    await add(FixedRectangle(camera.viewport.effectiveSize));
    return super.onLoad();
  }

  @override
  void onDragUpdate(int pointerId, DragUpdateInfo info) {
    _player.targetVector =
        info.eventPosition.viewport - camera.viewport.effectiveSize / 2;
    _player.targetVector.y *= -1;
  }

  @override
  void onDragEnd(int pointerId, DragEndInfo info) {
    _player.targetVector = Vector2.zero();
  }
}
